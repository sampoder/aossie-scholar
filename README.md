# Aossie-Scholar

The project is related to Google Scholar profiles and metrics. Many researchers have a Google Scholar profile. 
It is used by people to see how many papers a researcher has written, how many citations they have received, their h-index, i10-index... 
But these metrics are flawed. The goal of the project would be to extract information from Google Scholar and compute better metrics about a researcher's performance.
And then display this information and metrics with more fairer stats in another website.

## To run the app locally,
    # git remote add origin https://gitlab.com/aossie/aossie-scholar.git
    # git pull origin GSoC_2019
    # pip install -r requirements.txt
    # python manage.py runserver
    
   Now, you should be able to see the app running on your local server here -http://127.0.0.1:8000/metrics/
   One of the User Profile here- http://127.0.0.1:8000/metrics/m8dFEawAAAAJ/results/